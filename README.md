# Seja Bem Vindo Desenvolvedor ou Curioso

Este repositório contem lista de metodologias, padrões e regras a serem seguidas nos projetos que estiverem usando. Aqui também será lista a estrutura de trabalho e funcionamento de cada projeto, e contamos com sua ajuda pra abrir uma issue ou corrigir a wiki com as informações adequadamente. É muito importante todos ajudarem a manter as informações atualizadas, não temos um gerente de projeto (devops) apesar de termos o Fred e Luan como responsáveis pelos acesso eles não tem como fazerem tudo.

Algumas coisas para ajudar arrumar nossa wiki:

- Sempre que possível todos os projetos devem usar:
    + Git (sendo tudo salvo dentro do bitbucket da opera, se não possui acesso solicite);
    + GitFlow o uso do git pode vir a não ser obrigatório porém assim que terminar suas mudanças no branch develop tenha certeza de deixar o master e o develop atualizados;
    + Laravel é o framework php padrão adotado na agência (mas há projetos que ainda não estão com ele);
    + 
- Liquid Planner (LP), já sabe que tudo deve ficar comentado lá, esse repositório não é um substituto ao LP;

# Versionamento de Arquivos e Pastas

Ao versionar arquivos no git, lembre-se que arquivos de configurações não devem ser versionados, lembre-se sempre de adicionar seu arquivo de configuração no .gitignore. No uso Laravel o mesmo já tem um .gitignore que ajuda bastante use-o.

# Versão do PHP

PHP é uma ferramenta muito legal, prática e funcional. Todos queremos implementar novas funcionalidades e corrigir e melhorar sistemas, mas lembre-se sempre que antes de usar as novas funcionalidades do nosso querido PHP 7 ou qualquer outra versão tenha certeza que irá funcionar no ambiente de produção. Caso não saiba consulte o Fred, Luan ou seja lá quem for o responsável no momento.

# Como usar Markdown
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
